#!/usr/bin/env python
# coding: utf-8

import pandas as pd
import numpy as np
import re, gzip
from ReadFASTA_lib import GetFasta

class MOD:
    
    def list_all_modifications():  ## "initialize"
        mod_list = pd.read_csv('PTMs.txt', sep='\t')
        mod_list.set_index('name', inplace=True)
        return mod_list
    
    def __init__(self, aName, aSymbol, aState, aShift, aLcPrb, aCount):
        self.name = aName
        self.symbol = aSymbol
        self.state = aState
        self.shift = aShift
        self.lcprb = '___'
        self.count = 0
        ##check if lcprb/count is NaN
        if aLcPrb==aLcPrb:
            self.lcprb = aLcPrb
        if aCount==aCount:
            self.count = aCount
        self.lcprb_map = self.get_LcPrb()
        
    def get_ptm_attributes(row, m):
        ptm_attributes = []
        for _,i in m.iterrows():
            ID,symbol,state,shift = i
            if ID in row.index:  #checks if MOD exists in the data
                ptm_obj = MOD(ID, symbol, state, shift, row[ID+' Probabilities'], row[ID])
                ptm_attributes.append(ptm_obj)
        return ptm_attributes

    def get_LcPrb(self):
        x = self.lcprb
        lcprb_sites = dict()
        sobj = re.search(r'\(.+?\)', x)
        while sobj:
            pos = x.index('(') - 1
            p = float(sobj.group().strip(r'(|)'))
            lcprb_sites[pos] = p
            x = x.replace(sobj.group(),'',1)
            sobj = re.search(r'\(.+?\)', x)
        return lcprb_sites
    
    def are_localized(self, min_LcPrb):
        count_localized = 0
        for p in self.lcprb_map.values():
            if p >= min_LcPrb:
                count_localized += 1
        if self.count == count_localized:
            return True
        return False
    
    def get_ptm_attributes(row, m):
        ptm_attributes = []
        for _,i in m.iterrows():
            ID,symbol,state,shift = i
            if ID in row.index:  #checks if MOD exists in the data
                ptm_obj = MOD(ID, symbol, state, shift, row[ID+' Probabilities'], row[ID])
                ptm_attributes.append(ptm_obj)
        return ptm_attributes

    def all_localized(ptm_list, min_LcPrb):
        localization = True
        for i in ptm_list:
            localization = localization and i.are_localized(min_LcPrb)
            if not localization:
                break
        return localization
    
    def one_localized(ptm_list, min_LcPrb):
        localization = False
        for i in ptm_list:
            localization = localization or i.are_localized(min_LcPrb)
            if localization:
                break
        return localization


class PTMs_remapping:
    
    ###Getting intra-peptide modification site indexes
    def GetPtmPos(pep, inclusion_list):
        offset=0
        m_pos={}
        for match in re.finditer(r'(\(..\))', pep):
            if match.group().strip(")").strip("(") in inclusion_list:
                m_pos[match.start()-offset] = match.group().strip(")").strip("(")
            offset += (match.end()-match.start())
        return m_pos

    ###Getting intra-sequence peptide indexes
    def GetPepPos(pep,uniacc,fasta):
        try:
            seq = fasta[uniacc]['Seq']
        except:
            return -1
        pep = re.sub(r'\((..)\)', '', pep)
        pep = pep.replace("I","X").replace("L","X")
        seq = seq.replace("I","X").replace("L","X")
        match = re.search(pep, seq)
        if match:
            return match.start()
        return -1
    
    def MapPTMs(mapped_peps, fasta):
        if 'Peptide' not in mapped_peps.columns:
            print("ERROR!\nNo colum named 'Peptide' in the df passed to 'PTMs_remapping.MapPTMs' function")
            return np.nan
        M = MOD.list_all_modifications()
        mapped_ptms = []
        for _,row in mapped_peps.iterrows():
            ptms = PTMs_remapping.GetPtmPos(row.Peptide, list(M.symbol))
            clean_peptide = re.sub(r'\((.*?)\)', '', row.Peptide)
            clean_peptide = clean_peptide.replace("I","X").replace("L","X").strip('\n')
            for i in ptms.keys():
                new_row = list(row)
                new_row.append(clean_peptide)
                new_row.append(clean_peptide[i-1])
                new_row.append(i+row.Peptide_position)
                new_row.append(ptms[i])
                try:
                    padded_seq = 10*'_' + fasta[row['LeadProt']]['Seq'] + 10*'_'
                    new_row.append(padded_seq[i+row.Peptide_position-8+10:i+row.Peptide_position+7+10])
                except:
                    new_row.append(np.nan)
                mapped_ptms.append(new_row)
        new_cols = list(mapped_peps.columns) + ['AASeq','RES','POS','MOD','SeqWindow']
        return pd.DataFrame(mapped_ptms, columns=new_cols)
    
    def unique_counts_per_UniAcc(prot_list, unique_counts):
        return [unique_counts[_] if _ in list(unique_counts.keys()) else 0 for _ in prot_list]

    def get_leading(row):
        counts = row['unique_counts']
        accs = row['all_UniAcc']
        return accs[counts.index(max(counts))]

    def ambiguous_peptides(tmp, fasta): #the df passes must have a 'Peptide/PEPTIDE' column
        tmp.rename(columns={'PEPTIDE':'Peptide'}, inplace=True)
        if 'Peptide' not in tmp.columns:
            print("ERROR!\nNo colum named 'Peptide' in the df passed to 'PTMs_remapping.MapPTMs' function")
            return np.nan
        tmp['Proteins'] = tmp.Peptide.apply(lambda x: PTMs_remapping.assign_pep_to_prot(x, fasta) )
        tmp2 = []
        for _,row in tmp.iterrows():
            for i in row['Proteins'].split(';'):
                tmp2.append([i,row['Peptide']])
        tmp2 = pd.DataFrame(tmp2, columns=['n_proteins','Peptide'])
        prot_counts = tmp2.groupby('Peptide').count().reset_index()
        prot_counts['Amb_map'] = prot_counts.n_proteins.apply(lambda x: x>1)
        tmp3 = tmp.merge(prot_counts, on='Peptide')
        unique_mappings = tmp3[~tmp3.Amb_map]
        unique_mappings
        unique_counts = unique_mappings.groupby('Proteins').count()[['Peptide']].sort_values('Peptide', ascending=False)
        unique_counts.reset_index(inplace=True)
        unique_counts = unique_counts.set_index('Proteins').to_dict()['Peptide']
        tmp3['all_UniAcc'] = tmp3.Proteins.apply(lambda x: x.split(';'))
        tmp3['unique_counts'] = tmp3.all_UniAcc.apply(lambda x: PTMs_remapping.unique_counts_per_UniAcc(x,unique_counts))
        tmp3['LeadProt'] = tmp3.apply(PTMs_remapping.get_leading, axis=1)
        tmp3['GeneName'] = tmp3.LeadProt.apply(lambda x: PTMs_remapping.get_Gene(x, fasta))
        tmp3['All_entry_names']    = tmp3.all_UniAcc.apply(lambda x: [PTMs_remapping.get_Entry(_, fasta) for _ in x])
        tmp3['Uniprot_entry_name'] = tmp3.All_entry_names.apply(lambda x: x[0])
        tmp3['Peptide_position'] = tmp3.apply(lambda row: PTMs_remapping.GetPepPos(row['Peptide'],
                                                                                   row['LeadProt'],
                                                                                   fasta), axis=1)
        return tmp3[['Peptide','Peptide_position','LeadProt','GeneName','Uniprot_entry_name',
                     'all_UniAcc','All_entry_names','Amb_map']]
    
    def get_Entry(x, fasta):
        try:
            return fasta[x]['Entry']
        except:
            return np.nan
    def get_Gene(x, fasta):
        try:
            return fasta[x]['Gene']
        except:
            return np.nan

    def assign_pep_to_prot(pep, fasta):
        pep = re.sub(r'\(..\)','',pep)
        cleaned_pep = pep.replace("I","X").replace("L","X")
        matches = []
        for uniacc in fasta.keys():
            cleaned_seq = fasta[uniacc]['Seq'].replace("I","X").replace("L","X")
            match = re.search(cleaned_pep, cleaned_seq)
            if match:
                matches.append(uniacc)
        if len(matches)>0:
            matches.sort()
            return ';'.join(matches)
        else:
            return "?"
           
    def GetUniprotGenenames(uniprot_entries):
        '''
        Takes list of Uniprot accession numbers and
        returns a dictionary: {UniAcc => GeneName}
        '''
        import urllib
        from io import StringIO
        import pandas as pd
        # uniprot_ids2 = [_.split('-')[0] for _ in list(uniprot_ids)]
        url = 'https://www.uniprot.org/uploadlists/'  # This is the webserver to retrieve the Uniprot data
        params = {
            'from': "ENTRY",
            'to': 'GENENAME',
            'format': 'tab',
            'query': " ".join(list(uniprot_entries))
        }
        data = urllib.parse.urlencode(params)
        data = data.encode('ascii')
        request = urllib.request.Request(url, data)
        with urllib.request.urlopen(request) as response:
            res = response.read()
        df_fasta = pd.read_csv(StringIO(res.decode("utf-8")), sep="\t")
        return df_fasta.set_index('From').to_dict()['To']
        