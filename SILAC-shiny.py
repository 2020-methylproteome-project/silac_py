#!/usr/bin/env python
# coding: utf-8

# # SILAC_py
# 
# Developed by Enrico Massignani, PhD;
# 
# September 2021

# In[1]:


import pandas as pd
import numpy as np
import re, math, os, itertools, gzip
from datetime import datetime, date
from IPython.display import clear_output
from numpy.lib import scimath


# In[2]:


from ReadFASTA_lib import GetFasta
from MOD_lib import MOD, PTMs_remapping


# In[3]:


def make_pep_ratios_dictionary(fld, exps, ratios, 
                               min_Score, min_DScore, min_LcPrb,m):
    #filters
    evidence = pd.read_csv(os.path.join(fld,'evidence.txt'), sep='\t', low_memory=False)
    evidence = evidence[(evidence.Score > min_Score)&
                        (evidence.Reverse!='+')&
                        (evidence['Delta score'] > min_DScore)&
                        (evidence['Potential contaminant']!='+')&
                        (evidence.Charge>1)&
                        (evidence.Experiment.isin(exps))]
    evidence['Modified sequence'] = evidence['Modified sequence'].apply(lambda x: x.strip('_').replace('(ac)','').replace('(ox)','') )
    evidence['N_mods'] = evidence['Modified sequence'].apply(lambda x: x.count('('))
    evidence = evidence[evidence.N_mods>0]
    evidence.dropna(subset=ratios, how='all', inplace=True)
    evidence['MODS'] = evidence.apply(lambda x: MOD.get_ptm_attributes(x, m), axis=1)
    evidence['AreLocalized'] = evidence.MODS.apply(lambda x: MOD.all_localized(x, min_LcPrb))
    evidence = evidence[evidence.AreLocalized]
    #
    evidence.sort_values('Score', ascending=False, inplace=True)
    topscores = evidence[['Modified sequence','Score','m/z','Charge', 'Retention time']].copy(deep=True)
    topscores.drop_duplicates('Modified sequence', inplace=True)
    topscores.columns = ['Peptide','BestScore','mz','Charge', 'RetTime']
    #
    iterator = evidence.groupby(['Modified sequence','Experiment'])
    ratios_dict = {}
    for x,y in iterator:
        pep,exp = x
        try:
            ratios_dict[exp][pep] = y[ratios].apply(np.nanmedian)
        except:
            ratios_dict[exp] = {}
            ratios_dict[exp][pep] = y[ratios].apply(np.nanmedian)
    ## combine exps into a single 
    x = {}
    output = pd.DataFrame(columns=['Peptide'])
    for exp,ratios in ratios_dict.items():
        x[exp] = pd.DataFrame(ratios).T
        for c in x[exp].columns:
            x[exp].rename(columns={c: c+' '+exp+' peptide'}, inplace=True)
        x[exp].reset_index(inplace=True)
        x[exp].rename(columns={'index':'Peptide'}, inplace=True)
        output = output.merge(x[exp], on='Peptide', how='outer')
    # Add peptide Andromeda scores
    output = topscores.merge(output, on='Peptide')
    clear_output(wait=True)
    print("'make_pep_ratios_dictionary' complete!")
    return output.drop_duplicates()


# In[4]:


def make_unmodified_ratios_dictionary(fld, exps, ratios):
    #filters
    evidence = pd.read_csv(os.path.join(fld,'evidence.txt'), 
                           sep='\t', low_memory=False)
    evidence = evidence[(evidence.Score > 25)&
                        (evidence.Reverse!='+')&
                        (evidence['Delta score'] > 12)&
                        (evidence['Potential contaminant']!='+')&
                        (evidence.Charge>1)&
                        (evidence.Modifications=='Unmodified')&
                        (evidence.Experiment.isin(exps))]
    evidence['Modified sequence'] = evidence['Modified sequence'].apply(lambda x: x.strip('_').replace('(ac)','').replace('(ox)','') )
    evidence.dropna(subset=ratios, how='all', inplace=True)
    iterator = evidence.groupby(['Modified sequence','Experiment'])
    ratios_dict = {}
    for x,y in iterator:
        pep,exp = x
        Y = y[ratios].apply(np.nanmedian)
        try:
            ratios_dict[exp][pep] = Y
        except:
            ratios_dict[exp] = {}
            ratios_dict[exp][pep] = Y
    ## combine exps into a single 
    x = {}
    output = pd.DataFrame(columns=['Peptide'])
    for exp,ratios in ratios_dict.items():
        x[exp] = pd.DataFrame(ratios).T
        for c in x[exp].columns:
            x[exp].rename(columns={c: c+' '+exp+' peptide'}, inplace=True)
        x[exp].reset_index(inplace=True)
        x[exp].rename(columns={'index':'Peptide'}, inplace=True)
        output = output.merge(x[exp], on='Peptide', how='outer')
    clear_output(wait=True)
    print("'make_unmodified_ratios_dictionary' complete!")
    return output.drop_duplicates()


# In[5]:


def make_protein_ratios_df(fld, exps, ratios):
    pgroups = pd.read_csv(os.path.join(fld,'proteinGroups.txt'), 
                          sep='\t', low_memory=False)
    pgroups = pgroups[(pgroups['Potential contaminant']!='+')&
                      (pgroups['Reverse']!='+')&
                      (pgroups['Only identified by site']!='+')]
    cols = []
    for e in exps:
        for r in ratios:
            cols.append(r+' '+e)
    tmp = pd.DataFrame()
    for e in exps:
        pgroups[e+' filter'] = pgroups.apply(lambda row: (row['Peptides '+e] >= 2) and (row['Unique peptides '+e] >= 1), axis=1)
        cols2 = [c for c in cols if e in c ]
        pgroups[~pgroups[e+' filter']][cols2] = np.nan
    for _,row in pgroups.iterrows():
        ratio_row = pd.DataFrame(row[cols]).T
        for uniacc in row['Protein IDs'].split(';'):
            ratio_row['UniAcc'] = uniacc
            tmp = pd.concat((tmp,ratio_row), ignore_index=True)
    tmp.set_index('UniAcc', drop=True, inplace=True)
    for c in tmp.columns:
            tmp.rename(columns={c: c+' protein'}, inplace=True)
    clear_output(wait=True)
    print("'make_protein_ratios_df' complete!")
    return tmp.drop_duplicates().reset_index()


# In[6]:


def make_pep_to_prot_dictionary(fld):
    evidence = pd.read_csv(os.path.join(fld,'evidence.txt'), 
                           sep='\t', low_memory=False)
    tmp2 = evidence[['Modified sequence', 'Gene names', 'Leading razor protein',]].copy(deep=True)
    tmp2.drop_duplicates(inplace=True)
    tmp2['Modified sequence'] = tmp2['Modified sequence'].apply(lambda x: x.strip('_').replace('(ac)','').replace('(ox)','') )
    tmp2.rename(columns={'Modified sequence':'Peptide', 
                         'Gene names':'Gene', 
                         'Leading razor protein':'UniAcc'}, 
                inplace=True)
    return tmp2

## MAKE THE LIST OF COLUMNS TO EXTRACT FROM PROTEINGROUPS
def get_experiments_names(exps, inps, is_triple):
    if is_triple:
        ratios = ['Ratio M/L normalized', 
                  'Ratio H/L normalized', 
                  'Ratio H/M normalized']
    else:
        ratios = ['Ratio H/L normalized']
    ratios_list = []
    input_list = []
    for e in exps:
        for r in ratios:
            ratios_list.append(r+' '+e)
    input_list = []
    for i in inps:
        for r in ratios:
            input_list.append(r+' '+i)
    if is_triple:
        ratios_list = [
            ratios_list[0],
            ratios_list[4],
            ratios_list[1],
            ratios_list[3],
            ratios_list[2],
            ratios_list[5]
        ]
        input_list = [
            input_list[0],
            input_list[4],
            input_list[1],
            input_list[3],
            input_list[2],
            input_list[5]
        ]
    return ratios, ratios_list, input_list

def GetFwdRevPairs(ratios_list, is_triple):
    if is_triple:
        return [(ratios_list[0],ratios_list[1]),
                (ratios_list[2],ratios_list[3]),
                (ratios_list[4],ratios_list[5])]
    else:
        return [(ratios_list[0],ratios_list[1])]
    
def GetFwdRevPairsInput(input_list, is_triple):
    if is_triple:
        return [(input_list[0],input_list[1]),
                (input_list[2],input_list[3]),
                (input_list[4],input_list[5])]
    else:
        return [(input_list[0],input_list[1])]

def get_regulation_state(zscore, cutoff):
    '''
    Determines if peptide/protein is up or down-regulated.
    '''
    if np.isnan(zscore):
        return np.nan
    elif zscore > cutoff:
        return 1
    elif zscore < (0-cutoff):
        return -1
    else:
        return 0
    
def OverallRegulation(row,F,R):
    '''
    Compares Forward and Reverse experiment to check reproducibility.
    '''
    if np.isnan(row[F]) or np.isnan(row[R]):
        return np.nan
    elif row[F]==row[R]:
        return row[F]
    else:
        return np.nan
    
def get_labels_from_evidence(fld): #for the shiny app
    try:
        evidence = pd.read_csv(os.path.join(fld,'evidence.txt'), 
                                sep='\t', low_memory=False)
        options = list(set(evidence.Experiment))
        options.sort()
        return options
    except:
        return ["No 'evidence.txt' file found"]


# In[7]:


def AnalyseSILAC(fld,is_triple,fwdexp,revexp,fwdinp,revinp,fasta,cell_line,
                 min_Score=25, min_DScore=12, min_LcPrb=0.75, zscore_cutoff=2):
    start = datetime.now()
    seqs = {}
    GetFasta(fasta, seqs)
    M = MOD.list_all_modifications()

    ## GET DATA
    pep2prot = make_pep_to_prot_dictionary(fld)
    ratios, exp_ratios, input_ratios = get_experiments_names([fwdexp,revexp], [fwdinp,revinp], is_triple)
    pep_silac_ratios = make_pep_ratios_dictionary(fld, [fwdexp,revexp], ratios,
                                          min_Score,min_DScore,min_LcPrb, M)
    prot_silac_ratios = make_protein_ratios_df(fld, [fwdinp,revinp], ratios)
    peptidome = make_unmodified_ratios_dictionary(fld, [fwdexp,revexp], ratios)
    
    pep_silac_ratios.to_csv('tEST.csv')
    
    ## REVERT
    if is_triple:
        tmp = exp_ratios[-1]
        exp_ratios[-1] = tmp.replace('H/M','M/H')
        for df in [pep_silac_ratios, peptidome]:
            df[exp_ratios[-1]+' peptide'] = df[tmp+' peptide'].apply(lambda x: 1/x)
            df.drop(tmp+' peptide', axis=1, inplace=True)
        tmp = input_ratios[-1]
        input_ratios[-1] = tmp.replace('H/M','M/H')
        prot_silac_ratios[input_ratios[-1]+' protein'] = prot_silac_ratios[tmp+' protein'].apply(lambda x: 1/x)
        prot_silac_ratios.drop(tmp+' protein', axis=1, inplace=True)
    else:
        tmp = exp_ratios[-1]
        exp_ratios[-1] = tmp.replace('H/L','L/H')
        for df in [pep_silac_ratios, peptidome]:
            df[exp_ratios[-1]+' peptide'] = df[tmp+' peptide'].apply(lambda x: 1/x)
            df.drop(tmp+' peptide', axis=1, inplace=True)
        tmp = input_ratios[-1]
        input_ratios[-1] = tmp.replace('H/L','L/H')
        prot_silac_ratios[input_ratios[-1]+' protein'] = prot_silac_ratios[tmp+' protein'].apply(lambda x: 1/x)
        prot_silac_ratios.drop(tmp+' protein', axis=1, inplace=True)
    print(exp_ratios)
    fwd_rev_pairs  = GetFwdRevPairs(exp_ratios, is_triple)
    fwd_rev_pairs2 = GetFwdRevPairsInput(input_ratios, is_triple)

    ## SILAC PROTEINS
    for i in input_ratios:
        prot_silac_ratios[f'Log{i}'] = prot_silac_ratios[i+' protein'].apply(scimath.log2)
    stats2 = prot_silac_ratios.describe().loc[['mean','std']].to_dict()
    for i in input_ratios:
        mean = stats2[f'Log{i}']['mean']
        std  = stats2[f'Log{i}']['std']
        prot_silac_ratios[f'Z-Score {i}'] = prot_silac_ratios[f'Log{i}'].apply(lambda x: (x-mean)/std)
        prot_silac_ratios[f'Regulation {i}'] = prot_silac_ratios[f'Z-Score {i}'].apply(lambda x: 
                                                                                       get_regulation_state(x,zscore_cutoff))
    prot_ratios_cleaned = prot_silac_ratios[['UniAcc']+[f'Log{i}' for i in input_ratios]+
                                                  [f'Z-Score {i}' for i in input_ratios]+
                                                  [f'Regulation {i}' for i in input_ratios]]

    ## PEPTIDOME
    peptidome = peptidome.merge(pep2prot, on='Peptide').merge(prot_silac_ratios, 
                                                              on='UniAcc', how='left')
    for i,j in zip(exp_ratios, input_ratios):    
        peptidome[f'Log{i} pep/prot'] = peptidome[i+' peptide']/peptidome[j+' protein']
        peptidome[f'Log{i} pep/prot'] = peptidome[f'Log{i} pep/prot'].apply(scimath.log2)
    peptidome_cleaned = peptidome[['UniAcc','Gene','Peptide']+[f'Log{i} pep/prot' for i in exp_ratios]]
    stats = peptidome_cleaned.describe().loc[['mean','std']].to_dict()
    del peptidome

    ## SILAC PEPTIDES
    final_out = pep_silac_ratios.merge(pep2prot, on='Peptide')
    del pep_silac_ratios
    final_out = final_out.merge(prot_silac_ratios, 
                                on='UniAcc', how='left')
    for i,j in zip(exp_ratios, input_ratios):    
        final_out[f'Log{i} pep/prot'] = final_out[i+' peptide']/final_out[j+' protein']
        final_out[f'Log{i} pep/prot'] = final_out[f'Log{i} pep/prot'].apply(scimath.log2)
        mean = stats[f'Log{i} pep/prot']['mean']
        std  = stats[f'Log{i} pep/prot']['std']
        final_out[f'Z-Score {i}'] = final_out[f'Log{i} pep/prot'].apply(lambda x: (x-mean)/std)
        final_out[f'Regulation {i}'] = final_out[f'Z-Score {i}'].apply(lambda x: 
                                                                       get_regulation_state(x,zscore_cutoff))
    final_out_cleaned = final_out[['UniAcc','Gene','Peptide','BestScore','mz','Charge','RetTime']+
                                  [f'Log{i} pep/prot' for i in exp_ratios]+
                                  [f'Z-Score {i}' for i in exp_ratios]+
                                  [f'Regulation {i}' for i in exp_ratios]]
    del final_out, prot_silac_ratios
    for frpair in fwd_rev_pairs:
        forward = f'Regulation {frpair[0]}'
        reverse = f'Regulation {frpair[1]}'
        tmp = final_out_cleaned[['Peptide',forward,reverse]]
        overall = 'PAIR_'+forward
        tmp[overall] = tmp.apply(lambda row: OverallRegulation(row,forward,reverse),axis=1)
        final_out_cleaned = final_out_cleaned.merge(tmp[['Peptide',overall]],
                                                    on='Peptide', how='outer')
        
    for frpair in fwd_rev_pairs2:
        forward = f'Regulation {frpair[0]}'
        reverse = f'Regulation {frpair[1]}'
        tmp = prot_ratios_cleaned[['UniAcc',forward,reverse]]
        overall = 'PAIR_'+forward
        tmp[overall] = tmp.apply(lambda row: OverallRegulation(row,forward,reverse),axis=1)
        prot_ratios_cleaned = prot_ratios_cleaned.merge(tmp[['UniAcc',overall]],
                                                        on='UniAcc', how='outer')
    final_out_cleaned[f'Cell_line_{cell_line}'] = 'X'
    
    ## PRINT OUTPUT
    outdir = 'OUT-'+start.strftime("%Y-%m-%d-%H%M")
    try:
        os.mkdir(outdir) #creates output folder if not already present
    except:
        pass
    proteinsfile = os.path.join(outdir,'protein-ratios.csv')
    peptidomefile= os.path.join(outdir,'unmod-peptide-ratios.csv')
    peptidesfile = os.path.join(outdir,'MOD-peptide-ratios.csv')
    paramsfile   = os.path.join(outdir,'run-parameters.csv')
    pep_stats    = os.path.join(outdir,'Peptidome-stats.csv')
    prot_stats   = os.path.join(outdir,'Proteome-stats.csv')
    
    prot_ratios_cleaned.drop_duplicates(inplace=True)
    prot_ratios_cleaned.dropna(subset=[f'Log{i}' for i in input_ratios],
                                     how='all', inplace=True)
    peptidome_cleaned.drop_duplicates(inplace=True)
    peptidome_cleaned.dropna(subset=[f'Log{i} pep/prot' for i in exp_ratios],
                             how='all', inplace=True)
    final_out_cleaned.drop_duplicates(inplace=True)
    final_out_cleaned.dropna(subset=[f'Log{i} pep/prot' for i in exp_ratios],
                             how='all', inplace=True)
    print('# PRINT OUTPUT')
    prot_ratios_cleaned.drop_duplicates().to_csv(proteinsfile, index=False)
    peptidome_cleaned.drop_duplicates().to_csv(peptidomefile, index=False)
    # final_out_cleaned.drop_duplicates().to_csv(peptidesfile, index=False)
    
    print('Remapping PTMs ...')
    id_cols = ['Peptide','BestScore','mz','Charge','RetTime']
    ratio_cols = [_ for _ in final_out_cleaned.columns if 'Ratio' in _]
    cell_line_cols = [_ for _ in final_out_cleaned.columns if 'Cell_line_' in _]
    final_out_ratios = final_out_cleaned[id_cols+cell_line_cols+ratio_cols].drop_duplicates()
    final_out_mapped_peps = final_out_cleaned[['Peptide']].drop_duplicates()
    final_out_mapped_peps = PTMs_remapping.ambiguous_peptides(final_out_mapped_peps, seqs)
    final_out_mapped_peps = PTMs_remapping.MapPTMs(final_out_mapped_peps, seqs)
    
    final_out_mapped_peps = final_out_mapped_peps.merge(final_out_ratios, on='Peptide', how='right')
    final_out_mapped_peps.to_csv(peptidesfile, index=False)

    params = {'RunParameters':{
        'cell_line': cell_line,
        'fld': fld,
        'is_triple': is_triple,
        'fwdexp':fwdexp,
        'revexp':revexp,
        'fwdinp':fwdinp,
        'revinp':revinp,
        'fasta':fasta,
        'min_LcPrb':min_LcPrb,
        'min_Score':min_Score,
        'min_DScore':min_DScore,
        'zscore_cutoff':zscore_cutoff}
    }
    pd.DataFrame(stats).T.to_csv(pep_stats)
    pd.DataFrame(stats2).T.to_csv(prot_stats)
    pd.DataFrame(params).to_csv(paramsfile)
        
    end = datetime.now()
    print('DONE!')
    print(f'runtime = {end-start}')
    return f'runtime = {end-start}'
