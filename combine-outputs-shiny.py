#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import os, datetime, urllib.request
from ReadFASTA_lib import GetFasta
from MOD_lib import MOD, PTMs_remapping


# In[2]:


def UpdateCellPeps(cell_line_pep_lists, df):
    cell_line_cols = [_ for _ in df.columns if 'Cell_line_' in _]
    for i in cell_line_cols:
        try:
            cell_line_pep_lists[i] += list(set(df[~df[i].isnull()].Peptide))
        except:
            cell_line_pep_lists[i] = list(set(df[~df[i].isnull()].Peptide))


# In[3]:


def Combine(folderlist, fasta='Human_2021_01_isoforms.fasta.gz'):
    filelist = []
    for folder in folderlist:
        for file in os.scandir(folder):
            filelist.append(file.path)
    start = datetime.datetime.now()
    day = datetime.date.today().isoformat()
    outdir = f'COMBO-{day}'
    seqs = {}
    GetFasta(fasta, seqs)
    combined_files = []
    proteome = pd.DataFrame(columns=['UniAcc'])
    combo = pd.DataFrame(columns=['Peptide'])
    # combo_hm = pd.DataFrame(columns=['Peptide'])
    combo_id = pd.DataFrame(columns=['Peptide'])
    combo_id_hm = pd.DataFrame(columns=['Peptide','PROBABILITY_TRUE'])
    hmsilac_pep_list = []
    silac_pep_list = []
    cell_line_pep_lists = {}
    
    for file in filelist:
        if file.endswith('otein-ratios.csv'):
            print(file)
            tmp = pd.read_csv(file)
            proteome = proteome.merge(tmp, on='UniAcc', how='outer')
        if file.endswith('_summary.csv'):
            print(file)
            tmp = pd.read_csv(file).rename(columns={'PEPTIDE':'Peptide',
                                                    'SCORE':'hmSILAC_topscore',
                                                    'MZ':'hmSILAC_mz',
                                                    'CHARGE':'hmSILAC_charge',
                                                    'RETTIME':'hmSILAC_RT'})
            tmp = tmp[tmp.PREDICTION==1]
            hmsilac_pep_list += list(set(tmp.Peptide))
            id_cols = ['Peptide','H/L LOGRATIO','ME','DRT',
                       'PREDICTION','PROBABILITY_TRUE',
                       'hmSILAC_topscore','hmSILAC_mz','hmSILAC_charge','hmSILAC_RT']
            UpdateCellPeps(cell_line_pep_lists, tmp)
            combo = pd.concat((combo,tmp[['Peptide']].drop_duplicates()), ignore_index=True)
            combo_id_hm = pd.concat((combo_id_hm,tmp[id_cols]), ignore_index=True)
            combined_files.append(file)
        elif file.endswith('MOD-peptide-ratios.csv'):
            print(file)
            tmp = pd.read_csv(file).rename(columns={'PEPTIDE':'Peptide',
                                                    'BestScore':'SILAC_topscore',
                                                    'mz':'SILAC_mz',
                                                    'Charge':'SILAC_charge',
                                                    'RetTime':'SILAC_RT'})
            silac_pep_list += list(set(tmp.Peptide))
            id_cols = ['Peptide','SILAC_topscore','SILAC_mz','SILAC_charge','SILAC_RT']
            ratio_cols = [_ for _ in tmp.columns if 'Ratio' in _]
            UpdateCellPeps(cell_line_pep_lists, tmp)
            combo = combo.merge(tmp[['Peptide']+ratio_cols].drop_duplicates(), on='Peptide', how='outer')
            combo_id = pd.concat((combo_id,tmp[id_cols]), ignore_index=True)
            combined_files.append(file)
    combo.drop_duplicates('Peptide', inplace=True)
    print('Combined:',combo.shape)
    try:
        combo_id_hm.sort_values('PROBABILITY_TRUE', ascending=False, inplace=True)
        combo_id_hm.drop_duplicates('Peptide', inplace=True)
    except:
        pass
    try:
        combo_id.sort_values('SILAC_topscore', ascending=False, inplace=True)
        combo_id.drop_duplicates('Peptide', inplace=True)
    except:
        pass
    combo_id = combo_id.merge(combo_id_hm, on='Peptide', how='outer')
    print('Combined-identifications:',combo_id.shape)

    ## filter unchanging SILAC peps
    combo['hmSILAC_id'] = combo.Peptide.apply(lambda x: x in hmsilac_pep_list)
    combo['SILAC_id']   = combo.Peptide.apply(lambda x: x in silac_pep_list)
    tot_reg_cols = [_ for _ in combo.columns if _.startswith('PAIR_Regulation')]
    combo['isRegulated'] = combo[tot_reg_cols].apply(lambda row: sum([(_==1 or _==-1) 
                                                                      for _ in row]), axis=1)
    combo.isRegulated = combo.isRegulated > 0
    combo = combo[(combo.hmSILAC_id)|(combo.isRegulated)]
    print('Combined-regulation:',combo.shape)

    print('Remapping PTMs ...')
    mapped_peps = PTMs_remapping.ambiguous_peptides(combo[['Peptide']].drop_duplicates(), seqs)
    out_table = PTMs_remapping.MapPTMs(mapped_peps, seqs).merge(combo, on='Peptide')
    out_table['MOD_ID'] = out_table.LeadProt+'/'+out_table.RES+'/'+out_table.POS.apply(str)+'/'+out_table.MOD
    for line in cell_line_pep_lists.keys():
        out_table[line] = out_table.Peptide.apply(lambda x: x in cell_line_pep_lists[line])
    print('Print final output:', out_table.shape)
    try:
        os.mkdir(outdir)
    except:
        pass
    print('before merge:',out_table.shape)
    out_table = combo_id.merge(out_table, on='Peptide')
    print('after merge:',out_table.shape)
    essential_columns = ['hmSILAC_id','SILAC_id','isRegulated',
                         'Peptide','GeneName', 'LeadProt', 'Uniprot_entry_name', 
                         'all_UniAcc', 'All_entry_names', 'Amb_map', 
                         'RES', 'POS', 'MOD', 'MOD_ID', 'SeqWindow',
                         'PROBABILITY_TRUE',
                         'hmSILAC_topscore','hmSILAC_mz','hmSILAC_charge','hmSILAC_RT',
                         'SILAC_topscore',  'SILAC_mz',  'SILAC_charge',  'SILAC_RT']
    for i in essential_columns:
        if i not in out_table.columns: out_table[i]=np.nan
    out_table.to_csv(f'{outdir}/{day}-Combined-outputs.csv', index=False)
    out_table.to_csv(f'{outdir}/ProMetheusDB.csv', index=False)
    pd.DataFrame(combined_files, 
                 columns=['Combined files']).to_csv(f'{outdir}/{day}-Combined-files-list.csv')
    essential_columns += [_ for _ in out_table.columns if _.startswith('Cell_line')]
    essential_columns += [_ for _ in out_table.columns if _.startswith('PAIR_Regulation')]
    out_table = out_table[out_table.LeadProt!='?']
    out_table[essential_columns].to_csv(f'{outdir}/{day}-Combined-outputs-redux.csv', index=False)
    # Proteome
    proteome['Entry'] = proteome.UniAcc.apply(lambda x: PTMs_remapping.get_Entry(x,seqs) )
    proteome.set_index('Entry', inplace=True)
    proteome[proteome.index!='?'].to_csv(f'{outdir}/{day}-combined-proteome.csv')
    
    end = datetime.datetime.now()
    print(f'Runtime = {end-start}')
    return (f'{outdir}/{day}-Combined-outputs-redux.csv', f'Runtime = {end-start}')


# In[4]:


def MakeHtmlTable(path):
    data = pd.read_csv(path)
    ## to be removed from stable version ##
    for i in ['hmSILAC_topscore','hmSILAC_mz','hmSILAC_charge',
              'SILAC_topscore','SILAC_mz','SILAC_charge']:
        if i not in data.columns: data[i] = np.nan
    #########
    reg_cols = [_ for _ in data.columns if _.startswith('PAIR_Regulation')]
    line_cols = [_ for _ in data.columns if _.startswith('Cell_line')]
    cell_line_site_lists = {}
    for i in line_cols:
        try:
            cell_line_site_lists[i] += list(set(data[data[i]].MOD_ID))
        except:
            cell_line_site_lists[i] = list(set(data[data[i]].MOD_ID))
    peptable = data[['MOD_ID','Peptide',
                     'hmSILAC_id','hmSILAC_topscore','hmSILAC_mz','hmSILAC_charge','hmSILAC_RT',
                     'SILAC_id','SILAC_topscore','SILAC_mz','SILAC_charge','SILAC_RT',
                     'isRegulated']+reg_cols].drop_duplicates()
    for i in ['hmSILAC_id','SILAC_id','isRegulated']:
        peptable[i] = peptable[i].apply(str)
    sitetable = data[['LeadProt','GeneName','Uniprot_entry_name',
                      'RES','POS','MOD','MOD_ID','SeqWindow']].drop_duplicates()
    for line in cell_line_site_lists.keys():
        sitetable[line] = sitetable.MOD_ID.apply(lambda x: x in cell_line_site_lists[line])
    sitetable['N_cell_lines'] = sitetable[list(cell_line_site_lists.keys())].apply(sum,axis=1)
    sitetable['Peptides'] = sitetable.MOD_ID.apply(lambda x: list(set(peptable[peptable.MOD_ID==x].Peptide)) )
    
    return peptable,sitetable
